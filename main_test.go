package main

import (
	"bytes"
	"context"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"testing"
	"time"
)

type cmdParams struct {
	name    string
	args    []string
	env     []string
	timeout time.Duration
	stdOut  *bytes.Buffer
	stdErr  *bytes.Buffer
}

func TestMain(t *testing.T) {
	cmdList := strings.Split(`go run main.go`, " ")
	cmd := &cmdParams{
		name: cmdList[0],
		args: cmdList[1:],
		env: []string{
			fmt.Sprintf("%s=%s", "PWD", os.Getenv("PWD")),
		},
		timeout: 30 * time.Second,
		stdOut:  new(bytes.Buffer),
		stdErr:  new(bytes.Buffer),
	}
	if err := cmd.execute(); err != nil {
		t.Fatal(err)
	}
	if stdErr := cmd.stdErr.String(); stdErr != "" {
		t.Fatal(stdErr)
	}
	expectedString := "Hello world 😄 !"
	stdOut := cmd.stdOut.String()
	stdOut = strings.Trim(stdOut, "\n")
	if stdOut != expectedString {
		t.Fatalf(
			"For %#v\n expected %#v\n got %#v\n", cmd, expectedString, stdOut,
		)
	}
	fmt.Printf("Result: %v\n", cmd.stdOut.String())
}

func (param *cmdParams) execute() error {

	ctx, cancel := context.WithTimeout(context.Background(), param.timeout)
	defer cancel()

	cmd := exec.CommandContext(ctx, param.name, param.args...)

	cmd.Stdout = param.stdOut
	cmd.Stderr = param.stdErr
	cmd.Env = append(os.Environ(), param.env...)
	return cmd.Run()
}
